library(shiny)
library(plotly)
library(tuneR)
library(dplyr)

ui <- fluidPage(
   
   titlePanel("Proto interactive"),
   
   plotlyOutput('plotAudio'),
   
   plotlyOutput('plotAcc'),
   
   plotlyOutput('plotMag')
   
)

server <- function(input, output) {

  acc <- read.csv2("./zebre/ACCL_010120_002258.csv") 
  
  mag <- read.csv2("./zebre/MAG_010120_002258.csv")
  
  sound <- readWave("./zebre/AUDIO_010120_002258.2.wav")
  
  #sx <- round(length(sound@left) / sound@samp.rate * 1000)
  
  #sy <- sound@samp.rate
  
  #matrix(sound@left,nrow = sy,ncol = sx)
  
  output$plotAudio <- renderPlotly({
    p <- plot_ly(type = 'scatter') %>% 
      add_trace(x= 1:length(sound@left), y = sound@left, name = "wav", mode = 'lines') %>% 
      layout(title = 'Wav')
    
    p
  })
  
  output$plotAcc <- renderPlotly({
    
    acc2 <- acc %>% filter(relayouted())
    
    p <- plot_ly(type = 'scatter') %>% 
      add_trace(x= acc2$time, y = acc2$x, name = "x", mode = 'lines') %>% 
      add_trace(x= acc2$time, y = acc2$y, name = "y", mode = 'lines') %>% 
      add_trace(x= acc2$time, y = acc2$z, name = "z", mode = 'lines') %>%
      layout(title = 'ACCL')
    p
  })
  
  relayouted <- reactive({
    relayout <- event_data("plotly_relayout")
    if (is.null(relayout)) TRUE else if (is.null(relayout["xaxis.range[0]"][[1]])) between(acc$time, 0, last(acc$time)) else between(acc$time, relayout$`xaxis.range[0]`, relayout$`xaxis.range[1]`)
  })
  
  output$plotMag <- renderPlotly({
    
    mag2 <- mag %>% filter(relayouted())
    
    p <- plot_ly(type = 'scatter') %>% 
      add_trace(x= mag2$time, y = mag2$x, name = "x", mode = 'lines') %>% 
      add_trace(x= mag2$time, y = mag2$y, name = "y", mode = 'lines') %>% 
      add_trace(x= mag2$time, y = mag2$z, name = "z", mode = 'lines') %>%
      layout(title = 'MAG')
    
    p
  })

}

shinyApp(ui = ui, server = server)

