

index=0
yourfilenames=`ls ./$1/*.wav`
for entry in $yourfilenames
do
    echo "$entry"
    echo
    soundconverter -b -m audio/x-flac -s .flac $entry
    sox -S "${entry%.wav}".flac "${entry%.wav}".2.flac channels 2
    sox -b 8 -e signed-integer -S  "${entry%.wav}".2.flac  "${entry%.wav}".2.wav
done


